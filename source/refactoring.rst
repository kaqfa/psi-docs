Panduan Refactoring Aplikasi
=============================

Refactoring adalah pembenahan kode untuk lebih mudah terbaca dan lebih mudah di-manage. Ada beberapa kategori refactoring yang harus dilakukan yaitu:

- Workflow Refactoring
- Code Refactoring
- Database Refactoring