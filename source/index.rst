.. PSI-Documentation documentation master file, created by
   sphinx-quickstart on Sat Oct 21 06:27:28 2017.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Selamat datang di Dokumentasi PSI!
=============================================

.. toctree::
   :maxdepth: 2
   :caption: Daftar Isi:

    intro
    refactoring
    arsitektur
    sitemap

Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
